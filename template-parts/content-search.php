<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package orangegrove-base
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header">

		<div class="container">

			<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>

		</div>

	</header>

	<div class="container">

		<?php orangegrove_base_post_thumbnail(); ?>

		<div class="entry-summary">

			<?php the_excerpt(); ?>

		</div>

	</div>
	
</article>
