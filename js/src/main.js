/**
 * Theme JS - All the things
 */
jQuery(function () {
    App.init(); //Innit
});

var App = (function ($) {
    function init() {
        ogLoadMorePosts();
        testimonialSlider();
        iconGridSlider();
        bindMobileMenuPosition();
        mobileDropdownToggle();
    }

    /**
     * When mobile dropdown is clicked, toggle focus class
     */
    function mobileDropdownToggle() {
        $('#primary-menu .mobile-dropdown-toggle').click(function(e){
            e.preventDefault()
            e.stopPropagation()
            $(this).closest('li').toggleClass('focus')
        })
    }

    /**
     * Initialize Swiper slider
     */
    function testimonialSlider() {
        if (typeof Swiper !== 'undefined') {
            var swiper = new Swiper(".testimonial-slider__items", {
                slidesPerView: 1,
                slidesPerGroup: 1,
                autoHeight: true,
                spaceBetween: 20,
                pagination: {
                    el: ".testimonial-slider__controls .swiper-pagination",
                    clickable: true,
                },
            });
        }
    }

    /**
     * Initialize Icon Grid
     */
    function iconGridSlider() {
        if (typeof Swiper !== 'undefined') {
            var iconGridSwiper = new Swiper(".icon-content-panel__items.slider", {
                slidesPerView: 1,
                slidesPerGroup: 1,
                autoHeight: true,
                spaceBetween: 20,
                pagination: {
                    el: ".icon-content-panel__controls .swiper-pagination",
                    clickable: true,
                }
            });
        }
    }

    /**
     * Calls og_load_more_posts endpoint with attributes defined on .og-loadmore-button buttons
     */
    function ogLoadMorePosts() {

        //Get the button
        var loadMoreButton = $(".og-loadmore-button");

        //Replace items on filter change
        $('select.og-loadmore-filter').change(function () {
            var filterValue = $(this).val();
            var filterType = $(this).attr('data-filter-type');
            loadMoreButton.attr("data-refresh-all", true);
            loadMoreButton.attr("data-page", 1);
            switch (filterType) {
                case 'orderby':
                    loadMoreButton.attr("data-orderby", filterValue)
                    break;
                case 'taxonomy':
                    loadMoreButton.attr("data-taxonomy-term-slug", filterValue)
                    loadMoreButton.attr("data-taxonomy", $(this).attr('data-taxonomy'))
                    break;
                default:
                    break;
            }
            //Make the call
            load_more_posts(loadMoreButton);
        });

        loadMoreButton.click(function () {
            //Make the call
            load_more_posts(loadMoreButton);
        });

    }

    /**
     * Makes the AJAX call using attributes defined on the loadmore button button 
     */
    function load_more_posts(button) {
        var data = {
            action: "og_load_more_posts",
            partial: button.attr('data-partial'),
            post_type: button.attr('data-post-type'),
            page: button.attr("data-page"),
            posts_per_page: button.attr("data-posts-per-page"),
            category: button.attr("data-category-slug"),
            orderby: button.attr("data-orderby"),
            taxonomy: button.attr("data-taxonomy"),
            tax_term: button.attr("data-taxonomy-term-slug"),
            nonce: button.attr("data-nonce"),
        };

        jQuery.ajax({
            url: mainajax.ajaxurl,
            data: data,
            type: "POST",
            beforeSend: function (xhr) {
                //Add overlay to prevent multi-submits
                button.attr("disabled", "disabled");
                jQuery(".ajax-loading").show();
            },
            success: function (response) {

                button.removeAttr("disabled");

                if (response.data.html) {
                    //Replace or append?
                    if (button.attr("data-refresh-all") == 'true') {
                        jQuery(button.attr('data-cards-container')).html(response.data.html);
                    } else {
                        jQuery(button.attr('data-cards-container')).append(response.data.html);
                    }
                    //Increment pagination attribute
                    var pageNumber = parseInt(button.attr("data-page"));
                    button.attr("data-page", pageNumber + 1);
                }

                //Hide loadmore button if no more pages to load
                if (response.data.more_pages == false) {
                    button.hide();
                } else {
                    button.show();
                }
                //Disable refresh all (its set before calling when needed)
                button.attr("data-refresh-all", false);

                //Hide loading overlay
                jQuery(".ajax-loading").hide();
            },
            error: function () {
                //Just in-case
                jQuery(".ajax-loading").hide();
                button.hide();
            }
        });
    }

    //Set menu position on load and resize
    function bindMobileMenuPosition() {
        $(window).load(function () {
            setMobileMenuPosition();
        });

        $(window).resize(function () {
            clearTimeout(window.resizedFinished);
            window.resizedFinished = setTimeout(function () {
                setMobileMenuPosition();
            }, 250);
        });

        $('.menu-toggle').click(function(){
            setMobileMenuPosition();
        });

    }

    //Set the mobile menu absolute position
    function setMobileMenuPosition() {
        var headerHeight = parseInt($('.site-header').outerHeight())
        var $adminBar = $('div#wpadminbar');
        if ($adminBar.length == 0) {
            var adminBarHeight = 0;
        } else {
            var adminBarHeight = parseInt(jQuery('div#wpadminbar').outerHeight())
        }
        var menuTop = headerHeight + adminBarHeight;
        $('#primary-menu').css('top', menuTop);
        $('#primary-menu').css('max-height', 'calc(100% - '+menuTop+'px)');


    }

    return {
        init: init
    };

}(jQuery));
