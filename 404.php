<?php

/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package orangegrove-base
 */

get_header();
?>

<main id="primary" class="site-main">

	<section class="error-404 not-found">

		<div class="container">

			<div class="error-404__inner section-inner">

				<?php if ($title = get_field('404_title', 'option')) : ?>
					<h1 class="error-404__title"><?php echo $title; ?></h1>
				<?php endif; ?>

				<?php if ($subtitle = get_field('404_subtitle', 'option')) : ?>
					<p class="error-404__subtitle"><?php echo $subtitle; ?></p>
				<?php endif; ?>

				<?php get_search_form(); ?>

			</div>

		</div>

	</section>

</main>

<?php
get_footer();
