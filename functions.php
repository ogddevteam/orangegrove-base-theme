<?php
/**
 * orangegrove-base functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package orangegrove-base
 */

if ( ! defined( '_OG_THEME_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_OG_THEME_VERSION', '1.0.19' );
}

$files = [
    'lib/theme-setup/theme-init.php',                  //Theme init
    'lib/theme-setup/enqueue.php',                     //Theme scripts/styles
    'lib/theme-setup/acf-options-pages.php',           //ACF Options page
    'lib/utils/helpers.php',                           //Helper functions for theme
    'lib/utils/cleanup.php',                           //Remove WP bloat
    'lib/menus/register-menus.php',                    //Modify menus
    'lib/menus/menu-walkers.php',                      //Menu Walkers
];

foreach($files as $file) {
    include_once($file);
}
