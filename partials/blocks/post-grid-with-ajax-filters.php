<section class="post-grid-ajax-filters">

    <div class="container">

        <div class="post-grid-ajax-filters__inner section-inner">

            <div class="post-grid-ajax-filters__filters">

                <?php $terms = get_terms(['taxonomy' => $taxonomy_slug]); ?>

                <?php if ($terms && !is_wp_error($terms)) : ?>

                    <select class="og-loadmore-filter" name="category-filter" data-taxonomy="category" data-filter-type="taxonomy">
                        <option value=""><?php _e('Select a category', '@@text-domain'); ?></option>
                        <?php foreach ($terms as $term) : ?>
                            <option value="<?php echo $term->slug; ?>"><?php echo $term->name; ?></option>
                        <?php endforeach; ?>
                    </select>

                <?php endif; ?>

                <select class="og-loadmore-filter" name="orderby-filter" data-filter-type="orderby">
                    <option value="date"><?php _e('Sort by date', '@@text-domain'); ?></option>
                    <option value="alphabetical-asc"><?php _e('Sort alphabetically (A-Z)', '@@text-domain'); ?></option>
                    <option value="alphabetical-desc"><?php _e('Sort alphabetically (Z-A)', '@@text-domain'); ?></option>
                </select>

            </div>

            <div class="post-grid-ajax-filters__cards">

                <?php

                $query = new WP_Query($args);

                if ($query->have_posts()) {
                    while ($query->have_posts()) {
                        $query->the_post();

                        echo get_partial('partials/cards/card-example', [
                            'post_id' => get_the_ID(),
                        ]);
                    }
                }
                wp_reset_postdata();

                ?>

            </div>

            <div class="post-grid-ajax-filters__load-more">

                <button class="btn btn--tertiary og-loadmore-button" data-page="2" data-post-type="<?php echo $args['post_type']; ?>" data-posts-per-page="<?php echo $args['posts_per_page'] ?? 9; ?>" data-partial="card-insight" data-cards-container=".post-grid-ajax-filters__cards" data-taxonomy="" data-taxonomy-term-slug="" data-orderby="" data-refresh-all="false" data-nonce="<?php echo wp_create_nonce("og_load_posts_nonce"); ?>">

                    <?php _e('Load more insights', '@@text-domain');  ?>

                </button>

            </div>

        </div>

        <div class="ajax-loading">
            <div class="ajax-loading__inner">
                <div class="spinner"></div>
            </div>
        </div>

    </div>

</section>