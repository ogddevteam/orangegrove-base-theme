<section class="icon-content-panel">
    <div class="container">
        <div class="icon-content-panel__inner section-inner">
            <div class="icon-content-panel__items swiper<?php echo isset($variant) == 'slider' ? ' slider' : ' no-slider';?>">
                <div class="swiper-wrapper">

                    <?php while (have_rows('icon_content_panel')) : the_row(); ?>
                        <div class="swiper-slide">
                            <div class="icon-content-panel__item">
                                <?php if ($icon = get_sub_field('icon')) : ?>
                                    <img class="icon-content-panel__item-image skip-lazy" src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt']; ?>">
                                <?php endif; ?>
                                <?php if ($title = get_sub_field('title')) : ?>
                                    <h3 class="icon-content-panel__item-title"><?php echo esc_html($title); ?></h3>
                                <?php endif; ?>
                                <?php if ($content = get_sub_field('content')) : ?>
                                    <div class="icon-content-panel__item-content"><?php echo esc_html($content); ?></div>
                                <?php endif; ?>
                                <?php if ($cta = get_sub_field('cta')) : ?>
                                    <p class="icon-content-panel__item-cta"><a class="btn btn--primary" href="<?php echo esc_url($cta['url']); ?>" target="<?php echo esc_html($cta['target']); ?>"><?php echo esc_html($cta['title']); ?></a></p>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endwhile; ?>

                </div>

                <div class="icon-content-panel__controls">
                    <div class="swiper-pagination"></div>
                </div>
            </div>
        </div>
    </div>
</section>