<?php if (!have_rows('accordion')) {
    return;
} ?>

<section class="accordion">
    <div class="container">
        <div class="accordion__inner section-inner">
            <div class="js-accordion" data-accordion-cool-selectors="1">

                <?php while (have_rows('accordion')) : ?>

                    <?php the_row(); ?>
                    <?php $title = get_sub_field('question'); ?>
                    <?php $content = get_sub_field('answer'); ?>
                    <?php if ($title && $content) : ?>

                        <div class="js-accordion__item">
                            <h2 class="js-accordion__header">
                                <span class="accordion-header-title"><?php echo $title; ?></span>
                                <?php echo og_get_svg('chevron-down.svg'); ?>
                            </h2>
                            <div class="js-accordion__panel">
                                <?php echo $content; ?>
                            </div>
                        </div>

                    <?php endif; ?>

                <?php endwhile; ?>

            </div>
        </div>
    </div>
</section>