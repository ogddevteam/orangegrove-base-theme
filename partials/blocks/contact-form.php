<?php if (!$form_id = get_field('contact_form_id')) {
    return;
} ?>

<section class="contact-form">

    <div class="contact-form__inner section-inner">

        <div class="container">

            <?php echo do_shortcode('[gravityform id="' . $form_id . '" ajax="true" tabindex="49" description="false"]'); ?>

        </div>

    </div>

</section>