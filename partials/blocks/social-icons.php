<?php if(have_rows('social_icons', 'option')): ?>

    <ul class="social">

        <?php
        while ( have_rows('social_icons', 'option') ) : the_row();
        $social_channel = get_sub_field('social_channel', 'option');
        $social_url = get_sub_field('social_url', 'option');
        ?>

            <?php if(!empty($social_channel) && !empty($social_url)): ?>

                <li class="social__icon">
                    <a rel="nofollow noopener noreferrer" href="<?php echo $social_url; ?>" target="_blank">
                        <?php echo og_get_svg('social-' . $social_channel . '.svg'); ?>
                    </a>
                </li>

            <?php endif; ?>

        <?php endwhile; ?>

    </ul>

<?php endif; ?>