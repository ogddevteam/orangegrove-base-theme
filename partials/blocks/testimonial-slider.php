<?php if (!have_rows('testimonial_slider')) {
    return;
} ?>

<section class="testimonial-slider">
    <div class="container">
        <div class="testimonial-slider__inner section-inner">
            <div class="testimonial-slider__items swiper">
                <div class="swiper-wrapper">

                    <?php while (have_rows('testimonial_slider')) : the_row(); ?>
                        <div class="swiper-slide">
                            <div class="testimonial-slider__item ">
                                <?php if ($image = get_sub_field('image')) : ?>
                                    <img class="testimonial-slider__item-image skip-lazy" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
                                <?php endif; ?>
                                <?php if ($quote = get_sub_field('quote')) : ?>
                                    <div class="testimonial-slider__item-quote"><?php echo $quote; ?></div>
                                <?php endif; ?>
                                <?php if ($attribution = get_sub_field('attribution')) : ?>
                                    <p class="testimonial-slider__item-attribution"><?php echo $attribution; ?></p>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endwhile; ?>

                </div>

                <div class="testimonial-slider__controls">
                    <div class="swiper-pagination"></div>
                </div>

            </div>
        </div>
    </div>
</section>