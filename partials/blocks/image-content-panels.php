<section class="image-content-panel">
    <div class="image-content-panel__inner section-inner">

        <?php while (have_rows('image_content_panel')) : ?>

            <?php the_row(); ?>
            <?php $bleed = get_sub_field('bleed'); ?>
            <?php $reversed = get_sub_field('reversed'); ?>
            <?php $title = get_sub_field('title'); ?>
            <?php $content = get_sub_field('content'); ?>
            <?php $image = get_sub_field('image'); ?>

            <div class="image-content-panel__row<?php if(!empty($bleed)): echo " bleed"; endif; ?><?php if(!empty($reversed)): echo " reversed"; endif; ?>">
                <div class="container">

                    <?php if(!empty($image)): ?>

                        <div class="image-content-panel__image">
                            <?php echo wp_get_attachment_image($image['id'], 'full', false, [
                                'class' => 'lazy',
                                'loading' => true
                            ]); ?>
                        </div>

                    <?php endif; ?>

                    <?php if(!empty($content)): ?>

                        <div class="image-content-panel__content">

                            <?php if(!empty($title)): ?>

                                <h2><?php echo esc_html($title); ?></h2>

                            <?php endif; ?>

                            <?php echo esc_html($content); ?>

                        </div>

                    <?php endif; ?>

                </div>
            </div>

        <?php endwhile; ?>

    </div>
</section>