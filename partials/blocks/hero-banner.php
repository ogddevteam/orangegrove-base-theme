<section class="hero-banner">

    <?php if ($image = get_field('hero_banner_background_image')) : ?>
        <?php echo wp_get_attachment_image($image['id'], 'full', false, [
            'class' => 'hero-banner__image skip-lazy',
            'loading' => false
        ]); ?>

    <?php endif; ?>

    <div class="hero-banner__overlay"></div>
    <div class="hero-banner__inner section-inner">
        <div class="container">

            <?php if ($title = get_field('hero_banner_title')) : ?>
                <h1 class="hero-banner__title"><?php echo $title; ?></h1>
            <?php endif; ?>

            <?php if ($subtitle = get_field('hero_banner_subtitle')) : ?>
                <p class="hero-banner__subtitle"><?php echo $subtitle; ?></p>
            <?php endif; ?>

        </div>
    </div>
</section>