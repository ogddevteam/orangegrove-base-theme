// gulpfile.js
var gulp = require("gulp"),
    sass = require("gulp-sass"),
    postcss = require("gulp-postcss"),
    autoprefixer = require("autoprefixer"),
    cssnano = require("cssnano"),
    sourcemaps = require("gulp-sourcemaps"),
    minify = require("gulp-minify");

var paths = {
    sass : {
        source: {
            src: "sass/style.scss",
            dest: "."
        }
    },
    js : {
        source: {
            src: "js/src/main.js",
            dest: "./js/dest"
        }
    },
};

function style() {
    return (
        gulp
            .src(paths.sass.source.src)
            .pipe(sourcemaps.init())
            .pipe(sass())
            .on("error", sass.logError)
            .pipe(postcss([autoprefixer(), cssnano()]))
            .pipe(sourcemaps.write())
            .pipe(gulp.dest(paths.sass.source.dest))
    );
}

function script() {

    return gulp.src(paths.js.source.src, { allowEmpty: true }) 
        .pipe(minify({noSource: true}))
        .pipe(gulp.dest(paths.js.source.dest))
}

function watch() {
    gulp.watch("sass/**/*.scss", style);
    gulp.watch("js/src/main.js", script);
}
    
exports.style = style;
exports.script = script;
exports.watch = watch
