Orange Grove Base Theme
===

Hi. I'm the Orange Grove base theme, based on `_s`, or `underscores`.  I'm a theme meant for hacking so don't use me as a Parent Theme. Instead try turning me into the next, most awesome, WordPress theme out there. That's what I'm here for.

### Underscores features:

* A modern workflow with a pre-made command-line interface to turn your project into a more pleasant experience.
* A just right amount of lean, well-commented, modern, HTML5 templates.
* A custom header implementation in `inc/custom-header.php`. Just add the code snippet found in the comments of `inc/custom-header.php` to your `header.php` template.
* Custom template tags in `inc/template-tags.php` that keep your templates clean and neat and prevent code duplication.
* Some small tweaks in `inc/template-functions.php` that can improve your theming experience.
* A script at `js/navigation.js` that makes your menu a toggled dropdown on small screens (like your phone), ready for CSS artistry. It's enqueued in `functions.php`.
* 2 sample layouts in `sass/layouts/` made using CSS Grid for a sidebar on either side of your content. Just uncomment the layout of your choice in `sass/style.scss`.
Note: `.no-sidebar` styles are automatically loaded.
* Smartly organized starter CSS in `style.css` that will help you to quickly get your design off the ground.
* Full support for `WooCommerce plugin` integration with hooks in `inc/woocommerce.php`, styling override woocommerce.css with product gallery features (zoom, swipe, lightbox) enabled.
* Licensed under GPLv2 or later. :) Use it to make something cool.

### OG features:
* Structured includes from functions.php. Nice and simple.
* A very basic gulp setup for processing assets
* Generic AJAX load more functionality. See og-load-more.php and post-grid-with-ajax-filters.php
* Cleanup of WordPress cruft. See cleanup.php
* Helper functions to fetch SVGs and markup from the partials folder. See helpers.php
* Defer script function in enqueue.php
* One borrowed mixin for breakpoints. See scss/abstracts/mixins
* An overly complex SCSS folder. (@TODO)

Installation
---------------

### Requirements

- [Node.js](https://nodejs.org/)

### Quick Start

Clone or download this repository, change its name to something else (like, say, `acme`), then you'll need to a find/replace on the 
text domain and update the style.css information.

0. Search for "@@text-domain" and replace the text domain
1. Update the theme description in style.css 
2. Upload a theme screenshot. Add a 4:3 ration image called screenshot.png to the theme root
3. Next, update or delete this readme.

### Setup

To start using all the tools that come with the OG base theme you need to install the necessary Node.js dependencies :

```sh
$ npm install
```

### Available CLI commands

CLI Commands coming soon!

### Misc 

We favour the use of the the following packages at https://van11y.net/ for accessible components, including accordions, modals and tab panels.

For sliders, we prefer the use of slick https://kenwheeler.github.io/slick/

### Todo

See Asana Base Theme Project

### End
This is a WIP and all suggestion welcome.
Thanks and good luck! 


