<?php

/**
 * Template name: Kitchen Sink
 */
?>

<?php get_header(); ?>

<main id="primary" class="site-main">

    <?php echo get_partial('partials/blocks/hero-banner', []); ?>

    <?php echo get_partial('partials/blocks/accordion', []); ?>

    <?php echo get_partial('partials/blocks/testimonial-slider', []); ?>

    <?php echo get_partial('partials/blocks/contact-form', []); ?>

    <?php echo get_partial('partials/blocks/image-content-panels', []); ?>

    <?php echo get_partial('partials/blocks/icon-content-panels', ['variant' => 'slider']); ?>

    <?php echo get_partial('partials/blocks/icon-content-panels', []); ?>

</main>

<?php get_footer();
