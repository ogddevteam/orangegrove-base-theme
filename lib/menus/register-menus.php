<?php

// This theme uses wp_nav_menu() in one location.
register_nav_menus(
    array(
        'primary-menu' => esc_html__( 'Primary', '@@text-domain' ),
    )
);

// This theme uses wp_nav_menu() in one location.
register_nav_menus(
    array(
        'menu-footer' => esc_html__( 'Footer Menu', '@@text-domain' ),
    )
);
