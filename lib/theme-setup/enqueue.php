<?php

/**
 * Enqueue scripts and styles.
 */
function orangegrove_base_scripts()
{

	wp_enqueue_style('orangegrove-base-style', get_stylesheet_uri(), array(), _OG_THEME_VERSION);

	wp_style_add_data('orangegrove-base-style', 'rtl', 'replace');

	wp_enqueue_script('orangegrove-base-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _OG_THEME_VERSION, true);

	wp_enqueue_script('orangegrove-main', get_template_directory_uri() . '/js/dest/main-min.js', array('jquery'), _OG_THEME_VERSION, true);

	wp_enqueue_script('van11y-accordion', get_template_directory_uri() . '/js/dest/vendor/van11y-accessible-accordion-aria.min.js', array('jquery'), '3.0.1', true);

	wp_localize_script('orangegrove-main', 'mainajax', array('ajaxurl' => admin_url('admin-ajax.php')));

	//Only enqueue where required
	if (should_enqueue_vendor_files('swiper')) {
		wp_enqueue_style('swiper-style', get_template_directory_uri() . '/css/vendor/swiper-bundle.min.css', array(), '1.8.0');
		wp_enqueue_script('swiper-js', get_template_directory_uri() . '/js/dest/vendor/swiper-bundle.esm.browser.min.js', array('jquery'), '8.0.7', true);
	}
}
add_action('wp_enqueue_scripts', 'orangegrove_base_scripts');

/**
 * Defer JavaScript
 */
function og_defer_scripts($tag, $handle, $src)
{
	$defer = array(
		'orangegrove-base-navigation',
		'orangegrove-main',
		'swiper-js',
	);
	if (in_array($handle, $defer)) {
		return '<script src="' . $src . '" defer="defer"></script>' . "\n";
	}

	return $tag;
}
add_filter('script_loader_tag', 'og_defer_scripts', 10, 3);

/**
 * Conditional logic for loading scripts
 */
function should_enqueue_vendor_files($vendor)
{

	$page_templates = [
		'swiper' => [
			'page-templates/template-kitchen-sink.php',
		]
	];

	$post_types = [
		'swiper' => [
			// 'case-study',
		]
	];

	foreach ($page_templates[$vendor] as $template) {
		if (is_page_template($template)) {
			return true;
		}
	}

	foreach ($post_types[$vendor] as $post_type) {
		if (is_singular($post_type)) {
			return true;
		}
	}


	return false;
}
