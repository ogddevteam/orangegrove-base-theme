<?php

/**
 * A generic load more handler. 
 * 
 * Load more posts based on HTML attributes attached to a load more button. 
 * The JS is defined in main.js ogLoadMorePosts();
 */
function og_load_more_posts() {
    if (!wp_verify_nonce($_REQUEST['nonce'], "og_load_posts_nonce")) {
        wp_send_json_error([
            'message' => 'nonce check failed'
        ]);
        die();
    }

    $post_type = $_POST['post_type'];
    $taxonomy = $_POST['taxonomy'];
    $page = $_POST['page'];
    $posts_per_page = intval($_POST['posts_per_page']);
    $exclude_post = $_POST['exclude_post'];
    $tax_term = $_POST['tax_term'];
    $taxonomy_terms = $_POST['taxonomy_terms'];
    $partial = $_POST['partial'];
    $orderby = $_POST['orderby'];

    $allowed_post_types = [
        'post',
        'event_course',
        'case-study'
    ];

    if (!in_array($post_type, $allowed_post_types)) {
        wp_send_json_error(['message' => 'Post type not allowed']);
    }

    $args = array(
        'posts_per_page'   => $posts_per_page,
        'post_type'        => $post_type,
        'paged'            => $page,
        'order'            => 'DESC',
        'post__not_in'     => array($exclude_post),
    );

    if ($tax_term) {
        $args['tax_query'] = [[
            'taxonomy' => $taxonomy,
            'terms' => $tax_term,
            'field' => 'slug',
        ]];
    }

    if ($taxonomy_terms) {
        $args['tax_query'] = [];
        $args['tax_query']['relation'] = 'AND';
        foreach ($taxonomy_terms as $terms) {
            $args['tax_query'][] = [
                'taxonomy' => $terms[0],
                'field'    => 'slug',
                'terms'    => $terms[1]
            ];
        }
    }

    switch ($orderby) {
        case 'date':
            $args['orderby'] = 'date';
            $args['order'] = 'asc';
            # code...
            break;
        case 'alphabetical-asc':
            $args['orderby'] = 'title';
            $args['order'] = 'asc';
            # code...
            break;

        case 'alphabetical-desc':
            $args['orderby'] = 'title';
            $args['order'] = 'desc';
            # code...
            break;
        default:
            $args['orderby'] = 'date';
            break;
    }

    $query = new WP_Query($args);

    $count = 0;

    ob_start();

    if ($query->have_posts()) :

        while ($query->have_posts()) :
            $query->the_post();
            if ($partial == 'card-example') {
                echo get_partial('partials/cards/card-example', [
                    'post_id' => get_the_ID(),
                ]);
            }
            $count++;

        endwhile;
    else :
    // var_dump($args);
    // echo 'No posts';
    endif;

    if ($query->max_num_pages > $page) {
        $more_pages = true;
    } else {
        $more_pages = false;
    }

    wp_reset_postdata();

    wp_send_json_success([
        'html' => ob_get_clean(),
        'more_pages' => $more_pages,
        'post_count' => $query->found_posts
    ]);

    die();
}

add_action('wp_ajax_og_load_more_posts', 'og_load_more_posts');
add_action('wp_ajax_nopriv_og_load_more_posts', 'og_load_more_posts');
