<?php
/**
 * Load a partial and pass variables into it
 *
 * e.g. echo get_partial('partials/link', ['href'=> 'example.com', 'linktext' => 'Visit Example.com']);
 *
 * @param string $fileName  Name of the partial without .php appended
 * @param array  $variables Variables to make available in the partial
 *
 * @return string HTML
 */
function get_partial($fileName, $variables = array())
{
    foreach ($variables as $key => $value) {
        ${$key} = $value;
    }
    $filepath = realpath($fileName);
    ob_start();
    include get_template_directory() . '/' .$fileName . '.php';
    return ob_get_clean();
}

/**
 * Change a URI to a directory.
 * 
 * Used to fetch SVGs uploaded to the media library 
 * to a path for get_file_contents() - fetching
 * via a URI is very slow in comparison
 */
function og_upload_uri_to_path($url){
    preg_match('/.*(\/wp\-content\/uploads\/\d+\/\d+\/.*)/', $url, $mat);
    if(count($mat) > 0) return ABSPATH . $mat[1];
    return '';
}

/**
 * Dump and die
 */
function dd($var) {
    echo '<pre>';
    var_dump($var);
    echo '</pre>';
    die();
}

/**
 * Returns an SVG from images/svg
 * 
 * Usage echo og_get_svg('icon-twitter.svg');
 */
function og_get_svg($svg_path)
{
    $full_path = get_stylesheet_directory() . '/img/svg/';
    $full_path .=  $svg_path;

    if (!file_exists($full_path)) {
        return;
    }
    ob_start();
    include($full_path);
    return ob_get_clean();
}
