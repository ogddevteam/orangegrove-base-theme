<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package orangegrove-base
 */
?>

<footer id="colophon" class="site-footer">
	<div class="site-footer__top">
		<div class="container">
			<div class="site-footer__top-inner">
				<nav id="footer-navigation" class="site-footer__menu">

					<?php
					wp_nav_menu(
						array(
							'theme_location' => 'menu-footer',
							'menu_id'        => 'footer-menu',
							'depth'          => 5,
							'menu_class'     => 'footer-menu',
							'container_class' => 'footer-menu-container',
						)
					);
					?>

				</nav>
                <!-- Social icons list -->
                <?php echo get_partial('partials/blocks/social-icons', []); ?>
			</div>
		</div>
	</div>

	<div class="site-footer__bottom">
		<div class="container">
			<div class="site-footer__bottom-inner">

				<?php if ($accreditation = get_field('footer_accreditation', 'option')) : ?>
					<p class="site-footer__accreditation"><?php echo $accreditation; ?></p>
				<?php endif; ?>

				<a href="https://www.orangegrovedesigns.co.uk" target="__blank" rel="noopener noreferrer">Web design by OrangeGrove</a>

			</div>
		</div>
	</div>

</footer>

</div><!-- #page -->

<?php wp_footer(); ?>

</body>

</html>